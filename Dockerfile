FROM nginx:1.14

LABEL maintainer="kamal@ebi.ac.uk"

EXPOSE 80 8000

RUN rm /etc/nginx/conf.d/*

COPY ensembl-client-nginx/config/conf.d/local.conf /etc/nginx/conf.d/local.conf
COPY ensembl-client-nginx/config/conf.d/mime.types /etc/nginx/conf.d/mime.types

COPY src/ensembl/dist /usr/staticfiles

COPY ensembl-2020-static-assests/genome-images /usr/genomes-staticfiles/

